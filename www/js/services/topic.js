'use strict';

/**
 * @ngdoc function
 * @name VOA.services:TopicService
 * @description
 * # TopicService
 * Topic Service of the VOA app
 */

angular.module('VOA.services')
.factory('Topic', function(ENV, $resource, $log, $q, Settings,$http) {
  var topic;


 
  return {
   loadTopic: function(url) {
     var urlStr= ENV.api+url;
 

     var deferred = $q.defer();
     $http({
              method:'GET',
              url:urlStr
              }).success(function(response,header,config,status){
              //响应成功
                 var model={};        
                 var titile = $("#title h1",response).html();
               var content =$("#content",response).html();
                content= content.replace(/img(.*)src=\"(.*)\.(jpg|gif)"(.*)>/gi,"img"+"$1"+"src=\"http:\/\/www.51voa.com/"+"$2"+"."+"$3"+"\""+"$4"+">");
                
                    var mp3 =$("#mp3",response).attr("href");
                    var author=$("#content .byline",response).html();
                  var time=$("#content .datetime",response).html();
                
                  model.title=titile;
                   model.content=content;
                  model.mp3=mp3;
                 model.author=author;
                    model.time=time;
               
                deferred.resolve(model);

              }).error(function(data,header,config,status){
              //处理响应失败
              console.log("get data error");
                deferred.resolve(undefined);
              });
       
      return deferred;
    }
   
    }
});
