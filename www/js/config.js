"use strict";

 angular.module("VOA.config", [])

.constant("$ionicLoadingConfig", {
  "template": "请求中..."
})

.constant("ENV", {
  "version": "1.0.0",
  "name": "development",
  "debug": true,
  "api":'http://www.51voa.com/'
})

;