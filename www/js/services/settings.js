'use strict';

/**
 * @ngdoc function
 * @name VOA.services:SettingsService
 * @description
 * # SettingsService
 * Message Service of the VOA app
 */

angular.module('VOA.services')
.factory('Settings', function(ENV, $resource, $log, Storage) {
  var storageKey = 'settings';
  var settings = Storage.get(storageKey) || {
    sendFrom: false,
    saverMode: true
  };
  return {
    getSettings: function() {
      $log.debug('get settings', settings);
      return settings;
    },
    save: function() {
      Storage.set(storageKey, settings);
    }
  };
});
