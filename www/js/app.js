'use strict';

// Ionic VOA App
angular.module('VOA', [
  'ionic',
  'angularMoment',
  'VOA.controllers',
  'VOA.config']
)

.run(function($ionicPlatform, $log, $timeout, $state, $rootScope, amMoment, ENV) {

  // set moment locale
  amMoment.changeLocale('zh-cn');


  $ionicPlatform.ready(function() {
    if(window.cordova) {

      
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
    }

    
  });

})
.config(function(ENV, $stateProvider, $urlRouterProvider, $logProvider,$sceDelegateProvider) {

$sceDelegateProvider.resourceUrlWhitelist([
// Allow same origin resource loads.
'self',
// Allow loading from our assets domain. Notice the difference between * and **.
'http://all.51voa.com**']);

  $logProvider.debugEnabled(ENV.debug);
  $stateProvider
    .state('app', {
      url: '',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })
    
    .state('app.topics', {
      url: '/topics/:tab',
      views: {
        'menuContent': {
          templateUrl: 'templates/topics.html',
          controller: 'TopicsCtrl'
        }
      }
    })
    .state('app.topic', {
      url: '/topic/:type/:detail',
      views: {
        'menuContent': {
          templateUrl: 'templates/topic.html',
          controller: 'TopicCtrl'
        }
      }
    })
  $urlRouterProvider.otherwise('/topics/Technology_Report_1.html');
});

angular.module('VOA.controllers', ['VOA.services']);

angular.module('VOA.services', ['ngResource', 'VOA.config']);

