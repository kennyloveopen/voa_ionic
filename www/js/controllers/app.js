'use strict';

/**
 * @ngdoc function
 * @name VOA.controllers:AppCtrl
 * @description
 * # AppCtrl
 * Main Controller of the VOA app
 */

angular.module('VOA.controllers')
.controller('AppCtrl', function(ENV, $scope, $log, $timeout, $rootScope, $ionicPopup, $ionicLoading, Tabs,Settings) {
  $log.log('app ctrl');

  

  // environment config
  $scope.ENV = ENV;

  // ionic platform
  $scope.platform = ionic.Platform;

  
  // get user settings
  $scope.settings = Settings.getSettings();

  // error handler
  var errorMsg = {
    0: '网络出错啦，请再试一下',
    'wrong accessToken': '授权失败'
  };
  $rootScope.requestErrorHandler = function(options, callback) {
    return function(response) {
      var error;
      if (response.data && response.data.error_msg) {
        error = errorMsg[response.data.error_msg];
      } else {
        error = errorMsg[response.status] || 'Error: ' + response.status + ' ' + response.statusText;
      }
      var o = options || {};
      angular.extend(o, {
        template: error,
        duration: 1000
      });
      $ionicLoading.show(o);
      return callback && callback();
    };
  };

  

});
