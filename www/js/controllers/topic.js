'use strict';

/**
 * @ngdoc function
 * @name VOA.controllers:TopicCtrl
 * @description
 * # TopicCtrl
 * Topic Controller of the VOA app
 */

angular.module('VOA.controllers')
.controller('TopicCtrl', function($scope, $rootScope, $stateParams, $timeout, $ionicLoading, $ionicActionSheet, $ionicScrollDelegate, $log, Topics, Topic) {
  $log.debug('topic ctrl', $stateParams);


  // load topic data
  $scope.loadTopic = function(url) {
  
     var topicResource  = Topic.loadTopic(url);

    return topicResource.promise.then(function(response) {
      console.log(response);
        $scope.topic = response;
      }, $rootScope.requestErrorHandler({
        noBackdrop: true
      }, function() {
        $scope.loadError = true;
      })
    );
  };
  var type=$stateParams.type;
  var detail= $stateParams.detail;
  var url = type+"/"+detail;
  $scope.loadTopic(url);

  // do refresh

  $scope.doRefresh = function() {
   
        $scope.$broadcast('scroll.refreshComplete');
   
  };

});
